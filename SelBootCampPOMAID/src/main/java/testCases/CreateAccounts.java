package testCases;

import java.util.ArrayList;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseClass.BaseClass;
import pages.LoginPage;

public class CreateAccounts extends BaseClass{
	
	@BeforeTest
	public void setExcelSheet() {
	//location of the excel file for this test case
	excelSheetPath = "./data/CreateAccounts.xlsx";
	}
	
	@Test(dataProvider="fetchData")
	public void createAccounts(String uName, String password, String accName) {
		new LoginPage(driver)
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin()
		.clickWaffle()
		.clickViewAll()
		.clickSales()
		.clickAccountsTab()
		.clickNew()
		.enterAccountName(accName)
		.selectOwnerShipAsPublic()
		.clickSave()
		.verifyAccountCreated()
		.assertAll(); 
		
	}
}
