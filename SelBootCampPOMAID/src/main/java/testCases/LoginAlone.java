package testCases;

import java.util.ArrayList;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseClass.BaseClass;
import pages.LoginPage;

public class LoginAlone extends BaseClass{

	@BeforeTest
	public void setExcelSheet() {
	//location of the excel file for this test case
	excelSheetPath = "./data/SampleExcel.xlsx";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String uName, String password) {
		new LoginPage(driver)
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin();
	}
}
