package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import baseClass.BaseClass;
import pages.LoginPage;

public class EditAccounts extends BaseClass{
	@BeforeTest
	public void setExcelSheet() {
	//location of the excel file for this test case
	excelSheetPath = "./data/EditAccounts.xlsx";
	}
	
	@Test(dataProvider="fetchData")
	public void editAccounts(String uName, String password, String searchName, String billingStreet, String shippingStreet, String phoneNumber) {
		new LoginPage(driver)
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin()
		.clickWaffle()
		.clickViewAll()
		.clickSales()
		.clickAccountsTab()
		.searchByName(searchName)
		.selectFromSearchResults(searchName)
		.editAccount()
		.selectTypeAsTechnologyPartner()
		.selectIndustryAsHealthCare()
		.enterBillingStreet(billingStreet)
		.enterShippingStreet(shippingStreet)
		.selectCustomerPriorityAsLow()
		.selectSLAAsSilver()
		.selectActiveAsNo()
		.enterPhoneNumber(phoneNumber)
		.selectUpsellOpportunityAsNo()
		.clickSave()
		.verifyPhoneNumber(phoneNumber)
		.assertAll();
		
	}

}
